# WS Utel

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋

Para el funcionamiento de esta aplicación se requiere tenga instalado y configurados las siguientes herramientas:

- _[Python 3.7.9](https://www.python.org/downloads/release/python-379/ "Python 3.7.9")_
- _Algún entorno de desarrollo: [Pycharm](https://www.jetbrains.com/es-es/pycharm/download/ "Pycharm")_

### Instalación 🔧

_Lo primero que tenemos que configurar es agregar un intérprete de python a nuestro entorno de trabajo_
```
En PyCharm seleccione File –> Settings -> Project ->Python Interpreter
Le damos en la opción Add Python Interpreter y seleccionamos la carpeta 
donde deseamos crear nuestro entorno virtual, posteriormente seleccionamos 
la versión base de python con la que trabajaremos; en este caso las 2.7
```

_Ahora necesitamos instalar las librerias requeridas para el funcionamiento de la aplicación.
Para esto necesitamos instalar las librerias que se encuentran dentro 
del archivo de requirements.txt.
Simplemente se ejecuta el siguiente comando en la terminal:_

```
$ pip install -r requirements.txt
```

_El comando anterior debe ejecutarse en el directorio dónde se encuentra 
el archivo requirements.txt.
Haciendo esto, se instalarán en el sistema o en el virtualenv que tengamos 
activado, los paquetes anotados en el archivo requirements.txt_

### Ejecutando el proyecto  🔩
_Para poder iniciar con los pasos de instalación y configuración del servidor, es necesario contar con el archivo ".env" que contiene todas la información sensible necesaria para iniciar el servidor como conexiones a base de datos, contraseñas y usuarios. Para más información sobre este archivo póngase en contacto con:_

* **Daniel Palomino Maya** 
* **Iván Padilla Salgado**  

_Para ejecutar el proyecto es necesario haber realizado la instalación de las dependencias incluidas en el archivo de requirements._
_Una vez teniendo todo instalado y configurado se posiciona en la ruta donde está el proyecto, y se abre una consola de línea de comandos donde se introducirá el siguiente comando:_

```
./run_enrolment.sh
```

_Puede modificar ese archivo shell cambiando el parametro que representa al aula que va a procesar:_
```
echo "Ejecutando la aplicacion"
python3 main.py -a all

-a all: Indica que el proceso se correra para todas las aulas.
-a 01: Inidica que el proceso solo se correra para el aula deseada
```

_Puede modificar el archivo env para cambiar el ambiente donde correra el proceso:_
```
ENVIRONMENT= tipo_de_ambiente
BD_ENROLLMENT= bd

-Environment esperados-
Tipo de ambiente: dev, prod

-BD Enrolment-
Se utiliza este campo para poner el nombre de la tabla en la cual se va a realizar la actualización de la información.
```

## Construido con 🛠️

_Las herramientas utilizadas en este proyecto son:_

* [Python](https://docs.python.org/3.7/) - Lenguaje de Programación

## Versiónado 📌

Usamos [Bitbucket](https://bitbucket.org/) para el versionado. Para todas las versiones disponibles, mira los [tags en este repositorio](https://bitbucket.org/scalamx/ws_utel/src/master/).