# -*- coding: utf-8 -*-
import itertools
import logging
import pandas as pd

from config.settings import CHUNKSIZE, GET_ID_ENROLMENT, COLUMNAS_AULAS, ENROLMENT, ENVIRONMENT, DEV, PROD, \
    BD_ENROLLMENT
from config.sql import USUARIOS_AULA_ENROLMENT, VIEW_USUARIOS, DELETE_USUARIOS_ENROLMENT
from util.messages import print_message
from util.mysql import Conexion


class CreateEnrolment:

    def __init__(self, aula):
        self._aula = aula
        self._count = 0
        self._nombre_aula = 'moodle_aula_%s' % self._aula
        if ENVIRONMENT == DEV:
            bd_enrolment = ENROLMENT + "_" + DEV
            self._conexion_enrolment = Conexion(base_datos=DEV, bd_dev=bd_enrolment)
        elif ENVIRONMENT == PROD:
            self._conexion_enrolment = Conexion()

    def enrolment(self):
        try:
            df_usuarios_aula = self.consultar_usuarios_aula()
            if df_usuarios_aula.empty:
                print_message("No se encontraron registros en el aula")
            else:
                self.comparar_tablas_usuarios(df_usuarios_aula)
            print_message("Termino el proceso para el aula: " + str(self._nombre_aula) + "...")
        except Exception as error:
            logging.exception(error)

    def consultar_usuarios_aula(self):
        try:
            df_usuarios_aula = pd.DataFrame()
            print_message("Obteniendo lista de usuarios del aula: " + str(self._nombre_aula))
            _conexion_aula_moddle = Conexion(base_datos=self._nombre_aula)
            cursor = _conexion_aula_moddle.create_cursor()
            for chunk in pd.read_sql(sql=USUARIOS_AULA_ENROLMENT % (self._aula, self._aula), con=cursor,
                                     chunksize=CHUNKSIZE):
                df_usuarios_aula = pd.concat([df_usuarios_aula, chunk])
            return df_usuarios_aula
        except Exception as error:
            print_message("No se pudo establecer la conexion en el aula: " + str(self._nombre_aula))
            logging.exception(error)

    def comparar_tablas_usuarios(self, df):
        try:
            df_view = pd.DataFrame()
            cursor = self._conexion_enrolment.create_cursor()
            for chunk_view in pd.read_sql(sql=VIEW_USUARIOS % (BD_ENROLLMENT, self._aula), con=cursor, chunksize=CHUNKSIZE):
                df_view = pd.concat([df_view, chunk_view])
            tmp_df_origen = df
            tmp_df_destino = df_view
            df_key_origen = tmp_df_origen.drop(COLUMNAS_AULAS, axis=1)
            if tmp_df_destino.empty:
                print_message("No se encontraron registros para comparar, realizando la carga de los usuarios...")
                self.insertar_usuarios_enrolment(df_to_insert=df_key_origen, df_origen=df,
                                                 conexion=self._conexion_enrolment)
            elif len(tmp_df_origen.index) > 0 and len(tmp_df_destino.index) > 0:
                df_destino_without_id = tmp_df_destino.drop(['id'], axis=1)
                df_key_destino = df_destino_without_id.drop(COLUMNAS_AULAS, axis=1)
                df_merge = df_key_destino.merge(df_key_origen, how='outer', indicator='union')
                df_delete = df_merge[df_merge.union == 'left_only']
                df_insert = df_merge[df_merge.union == 'right_only']
                if len(df_delete.index) > 0 or len(df_insert.index) > 0:
                    if len(df_delete.index) > 0:
                        self.eliminar_usuarios_enrolment(df_to_delete=df_delete, df_destino=df_view,
                                                     conexion=self._conexion_enrolment)
                    if len(df_insert.index) > 0:
                        self.insertar_usuarios_enrolment(df_to_insert=df_insert, df_origen=df,
                                                        conexion=self._conexion_enrolment)
                else:
                    print_message("No se encontraron registros que actualizar...")
        except Exception as error:
            logging.exception(error)

    def insertar_usuarios_enrolment(self, df_to_insert, df_origen, conexion):
        try:
            if len(df_to_insert.index) > 0:
                print_message("Insertando los nuevos registros del: " + str(self._nombre_aula) + " en la tabla enrollment")
                df_usuarios_insertar = self.filtrar_df(df_keys=df_to_insert, df_filter=df_origen)
                df_insertar = df_usuarios_insertar.drop(['key_enrolment'], axis=1)
                engine = conexion.create_engine()
                print_message("El total de registros del aula a insertar es: " + str(df_insertar.shape[0]))
                for chunk in self.chunker(df_insertar, CHUNKSIZE):
                    chunk.to_sql(name=BD_ENROLLMENT, con=engine, if_exists="append", index=False, method="multi")
                print_message("Registros insertados correctamente...")
            else:
                print_message("No se encontraron registros para insertar...")
        except Exception as error:
            logging.exception(error)

    def eliminar_usuarios_enrolment(self, df_to_delete, df_destino, conexion):
        try:
            if len(df_to_delete.index) > 0:
                print_message("Eliminando los registros que no se encuentran en el aula: " + str(self._nombre_aula) +
                              " de la tabla enrollment")
                df_usuarios_eliminar = self.filtrar_df(df_keys=df_to_delete, df_filter=df_destino)
                df_filter_id = df_usuarios_eliminar.drop(GET_ID_ENROLMENT, axis=1)
                list_ids_enrolment = self.convertir_df_to_list(df_filter_id)
                ids_list = ','.join(str(value) for value in list_ids_enrolment)
                if ids_list:
                    print_message("El total de registros del aula a eliminar es: " + str(df_filter_id.shape[0]))
                    resultado = conexion.ejecutar(DELETE_USUARIOS_ENROLMENT % (BD_ENROLLMENT, ids_list))
                    if resultado:
                        print_message("Registros eliminados...")
                    else:
                        print_message("Hubo un error al eliminar los registros...")
                else:
                    print_message("No se encontraron registros para eliminar...")
            else:
                print_message("No se encontraron registros para eliminar...")
        except Exception as error:
            logging.exception(error)

    def filtrar_df(self, df_keys, df_filter):
        if 'union' in df_keys.columns:
            df_filter_key = df_keys.drop(['union'], axis=1)
        else:
            df_filter_key = df_keys
        keys = self.convertir_df_to_list(df_filter_key)
        df_resultado = df_filter[(df_filter.key_enrolment.isin(keys))]
        return df_resultado

    @staticmethod
    def chunker(seq, size):
        return (seq[pos: pos + size] for pos in range(0, len(seq), size))

    @staticmethod
    def convertir_df_to_list(df):
        list_key = df.values.tolist()
        keys = list(itertools.chain(*list_key))
        return keys
