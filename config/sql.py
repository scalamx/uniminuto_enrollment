USUARIOS_AULA_ENROLMENT = """
SELECT
mdl_user.id AS userid,
mdl_user.username AS username,
mdl_user.firstname AS firstname,
UPPER(mdl_user.email) AS email,
mdl_course.id as courseid,
mdl_course.fullname as fullname,
%s as idAula,
mdl_user_enrolments.timecreated as timestamp,
'Uniminuto' as groups_name,
mdl_user.lastname AS lastname,
mdl_course.shortname as courseshortname,
mdl_user.suspended as suspendido,
mdl_groups_members.groupid as groupid,
concat(mdl_user.id, '_', mdl_user.username, '_', UPPER(mdl_user.email), '_', mdl_course.id, '_', %s) as key_enrolment
FROM mdl_user
INNER JOIN 	mdl_user_enrolments ON mdl_user.id = mdl_user_enrolments.userid
INNER JOIN 	mdl_enrol ON mdl_enrol.id = mdl_user_enrolments.enrolid
INNER JOIN 	mdl_course ON mdl_course.id = mdl_enrol.courseid
LEFT JOIN 	mdl_groups ON mdl_groups.courseid = mdl_course.id
LEFT JOIN 	mdl_groups_members ON mdl_user.id = mdl_groups_members.userid AND mdl_groups.id = mdl_groups_members.groupid
LEFT JOIN 	mdl_context ON mdl_context.instanceid = mdl_groups.courseid AND mdl_context.contextlevel = 50
LEFT JOIN	mdl_role_assignments ON mdl_user.id = mdl_role_assignments.userid AND mdl_role_assignments.contextid = mdl_context.id
LEFT JOIN	mdl_role ON mdl_role_assignments.roleid = mdl_role.id
WHERE
mdl_user_enrolments.`status` = 0
AND
mdl_course.visible=1
GROUP BY
mdl_user.id,
mdl_course.id
"""

VIEW_USUARIOS = """
SELECT
id,
userid,
username,
firstname,
email,
courseid,
fullname,
idAula,
timestamp,
groups_name,
lastname,
courseshortname,
suspendido,
groupid,
concat(userid, '_', username, '_', upper(email), '_', courseid, '_', idAula, '_', groupid) AS key_enrolment
FROM %s
WHERE idAula = %s
"""

DELETE_USUARIOS_ENROLMENT = """
DELETE FROM %s WHERE id in (%s)
"""