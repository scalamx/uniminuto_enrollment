# -*- coding: utf-8 -*-
import os
from os.path import join, dirname
from dotenv import load_dotenv, find_dotenv

dotenv_path = join(dirname(__file__), '../.env')
load_dotenv(find_dotenv())

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
ENROLMENT = 'enrolment'
DEV = 'dev'
PROD = 'prod'
COMPLETO = 'all'
BD_ENROLLMENT = os.environ.get("BD_ENROLLMENT")
DEFAULT = ''
ENVIRONMENT = os.environ.get("ENVIRONMENT")
CHUNKSIZE = int(os.environ.get("CHUNK_SIZE"))
FILE_OUTPUT = 'output_enrollment.txt'

COLUMNAS_AULAS = ['userid', 'username', 'firstname', 'email', 'courseid', 'fullname', 'idAula', 'timestamp',
                  'groups_name', 'lastname', 'courseshortname', 'suspendido', 'groupid']
GET_ID_ENROLMENT = ['userid', 'username', 'firstname', 'email', 'courseid', 'fullname', 'idAula', 'timestamp',
                    'groups_name', 'lastname', 'courseshortname', 'suspendido', 'groupid', 'key_enrolment']
LISTA_AULAS = ('01', '01')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get("DATABASE_DEFAULT_NAME"),
        'USER': os.environ.get("DATABASE_DEFAULT_USER"),
        'PASSWORD': os.environ.get("DATABASE_DEFAULT_PASSWORD"),
        'HOST': os.environ.get("DATABASE_DEFAULT_HOST"),
        'PORT': os.environ.get("DATABASE_COMMON_PORT_NO_SECURITY"),
    },
    'dev': {
        'ENGINE': 'mysql_server_has_gone_away',
        'USER': os.environ.get("DATABASE_DEV_USER"),
        'PASSWORD': os.environ.get("DATABASE_DEV_PASSWORD"),
        'HOST': os.environ.get("DATABASE_DEV_HOST"),
        'PORT': os.environ.get("DATABASE_COMMON_PORT_NO_SECURITY"),
    },
    'moodle_aula_01': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ.get("DATABASE_MOODLE_AULA1_NAME"),
        'USER': os.environ.get("DATABASE_COMMON_USER_MODDLE"),
        'PASSWORD': os.environ.get("DATABASE_COMMON_PASSWORD_MODDLE"),
        'HOST': os.environ.get("DATABASE_COMMON_HOST_MODDLE_1_6"),
        'PORT': os.environ.get("DATABASE_COMMON_PORT_NO_SECURITY"),
    }
}
