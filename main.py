# -*- coding: utf-8 -*-
import argparse
import os

from enrollment.create_enrolment import CreateEnrolment
from util.messages import print_message
from config.settings import LISTA_AULAS, FILE_OUTPUT, COMPLETO


def process():
    if args.aula == COMPLETO:
        for _aula in LISTA_AULAS:
            print_message('Inicio el proceso para el aula: ' + str(_aula))
            enrolment = CreateEnrolment(aula=_aula)
            enrolment.enrolment()
            print_message('Finalizo el proceso para el aula: ' + str(_aula))
    else:
        print_message('Inicio el proceso para el aula: ' + str(args.aula))
        enrolment = CreateEnrolment(aula=args.aula)
        enrolment.enrolment()
        print_message('Finalizo el proceso para el aula: ' + str(args.aula))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--aula", help="Numero de aula a ejecutar")
    args = parser.parse_args()
    if os.path.exists(FILE_OUTPUT):
        os.remove(FILE_OUTPUT)
    print_message('Iniciando el proceso de actualizacion de enrollment')
    process()
    print_message('Finalizo el proceso de actualizacion de enrollment')
