#!/bin/bash

echo "Iniciando la maquina virtual..."
source enrollment-env/bin/activate

echo "Ejecutando la aplicacion"
python3 main.py -a all

echo "Apagando la maquina virutal..."
deactivate